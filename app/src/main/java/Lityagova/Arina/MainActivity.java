package Lityagova.Arina;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.MenuItem;
import android.widget.EditText;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.widget.Toolbar.OnMenuItemClickListener;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.BottomNavigationView.OnNavigationItemSelectedListener;

import java.io.File;

import petrov.kristiyan.colorpicker.ColorPicker;
import Lityagova.Arina.R;
import yogesh.firzen.filelister.FileListerDialog;
import yogesh.firzen.filelister.OnFileSelectedListener;

public class MainActivity extends AppCompatActivity {

    private String name = "Новый рисунок";

    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbarView = (Toolbar) findViewById(R.id.topBarView);

        toolbarView.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_new:
                        newPaint();
                        break;

                }
                return true;
            }
        });


        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener( new OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.action_brush:
                        DrawingView.setBrush();
                        break;

                    case R.id.action_rubber:
                        DrawingView.setRubber();
                        break;

                    case R.id.action_color:
                        newColor();
                        break;
                }

                return true;
            }
        });
    }

    private void newPaint() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Введите новое название");

        final EditText input = new EditText(this);

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);


        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toolbar toolbarView = (Toolbar) findViewById(R.id.topBarView);
                name = input.getText().toString();
                toolbarView.setTitle(name);
                DrawingView.clean();
            }
        });
        builder.setNegativeButton("Закрыть", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void newColor() {
        ColorPicker colorPicker = new ColorPicker(MainActivity.this);
        colorPicker.show();
        colorPicker.setOnChooseColorListener(new ColorPicker.OnChooseColorListener() {
            @Override
            public void onChooseColor(int position, int color) {
                DrawingView.setColor(color);

                BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigationView);
                switch (DrawingView.getLastTool()) {
                    case 1:
                        bottomNavigationView.setSelectedItemId(R.id.action_brush);
                        DrawingView.setBrush();
                        break;
                    case 2:
                        bottomNavigationView.setSelectedItemId(R.id.action_rubber);
                        DrawingView.setRubber();
                        break;
                }
            }

            @Override
            public void onCancel(){

            }
        });
    }
}